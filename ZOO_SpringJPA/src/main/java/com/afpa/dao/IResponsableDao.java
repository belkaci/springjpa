package com.afpa.dao;

import java.util.List;


import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.afpa.entity.Responsable;

	//definition de l'interface IAnimalDao qui implemente l'interface Repository 
	// l'interface Repository prend Animal comme type d'Entit� et Integer comme type pour l'identifiant

	@Repository
	public interface IResponsableDao extends CrudRepository<Responsable,Integer> {


		// je definis la METHODE findByNomOrderByNom qui prend en parametre p
		// un string entr� dans nomParam et qui renvoie une liste de Responsables
		List<Responsable> findByNomOrderByNomResponsable(@Param("nomParam")String p);


		// je definis la METHODE findAllByOrderByNomDesc qui renvoie la liste des Responsables
		// par ordre croissant

		//		@Query("select r from Responsable an order by r.nom asc")
		List<Responsable> findAllByOrderByNomDesc();


		List<Responsable> findByNom(String nom);

	}