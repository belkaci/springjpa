package com.afpa.dao;
import java.util.List;


import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.afpa.entity.Aliment;



	//definition de l'interface IAlimentDao qui implemente l'interface Repository 
	// l'interface Repository prend Aliment comme type d'Entit� et Integer comme type pour l'identifiant

	@Repository
	public interface IAlimentDao extends CrudRepository<Aliment,Integer> {


		// je definis la METHODE findByNomOrderByNomAliment qui prend en parametre p
		// un string entr� dans nomParam et qui renvoie une liste d'animaux	
		List<Aliment> findByNomOrderByNomAliment(@Param("nomParam")String p);


		// je definis la METHODE findAllByOrderByNomDesc qui renvoie la liste des animaux
		// par ordre croissant

		//		@Query("select al from Aliment an order by al.nom asc")
		List<Aliment> findAllByOrderByNomDesc();

	}
	


