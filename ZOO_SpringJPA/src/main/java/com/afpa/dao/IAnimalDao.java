package com.afpa.dao;
import java.util.List;


import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.afpa.entity.Animal;

	//definition de l'interface IAnimalDao qui implemente l'interface Repository 
	// l'interface Repository prend Animal comme type d'Entit� et Integer comme type pour l'identifiant

	@Repository
	public interface IAnimalDao extends CrudRepository<Animal,Integer> {


		// je definis la METHODE findByNomOrderByRaceAnimal qui prend en parametre p
		// un string entr� dans raceParam et qui renvoie une liste d'animaux	
		List<Animal> findByNomOrderByRaceAnimal(@Param("raceParam")String p);


		// je definis la METHODE findAllByOrderByNomDesc qui renvoie la liste des animaux
		// par ordre croissant

		//		@Query("select r from Animal an order by r.race asc")
		List<Animal> findAllByOrderByRaceDesc();

	}

