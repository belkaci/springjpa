package com.afpa.dao;
import java.util.List;


import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.afpa.entity.Animal;
import com.afpa.entity.Enclos;

	//definition de l'interface IAnimalDao qui implemente l'interface Repository 
	// l'interface Repository prend Enclos comme type d'Entit� et Integer comme type pour l'identifiant

	@Repository
	public interface IEnclosDao extends CrudRepository<Enclos,Integer> {


		// je definis la METHODE findByNomOrderByNom qui prend en parametre p
		// un string entr� dans nomParam et qui renvoie une liste d'Enclos
		List<Enclos> findByNomOrderByNomEnclos(@Param("nomParam")String p);


		// je definis la METHODE findAllByOrderByNomDesc qui renvoie la liste des enclos
		// par ordre croissant

		//		@Query("select e from Enclos an order by e.nom asc")
		List<Enclos> findAllByOrderByNomDesc();
		
		List<Enclos> findByNom(String nom);
		Enclos findNom(String nom);
	
		}
		
		
		
		
		

	

