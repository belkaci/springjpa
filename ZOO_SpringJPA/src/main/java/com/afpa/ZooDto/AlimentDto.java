package com.afpa.ZooDto;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@ToString
@Getter
@Setter
public class AlimentDto {

	private String nom_aliment;
	private int id_aliment;
	
	

}
