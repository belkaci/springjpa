package com.afpa.ZooDto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@ToString
@Getter
@Setter
public class AnimalDto {

	private int id;
	private String race;
	private int age;

}
