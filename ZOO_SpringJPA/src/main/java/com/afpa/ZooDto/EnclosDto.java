package com.afpa.ZooDto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
@Builder
@ToString
@Getter
@Setter

public class EnclosDto {

	private String enclos_race;
	private int id_enclos;

}