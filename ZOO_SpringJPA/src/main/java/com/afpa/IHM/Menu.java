package com.afpa.IHM;

import java.util.List;
import java.util.Scanner;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.afpa.ZooDto.AlimentDto;
import com.afpa.ZooDto.AnimalDto;
import com.afpa.conf.SpringConfiguration;
import com.afpa.service.IAlimentService;
import com.afpa.service.IAnimalService;
import com.afpa.service.IEnclosService;
import com.afpa.service.IResponsableService;

public class Menu {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
		ctx.register(SpringConfiguration.class);
		ctx.refresh();

		IAnimalService animalService = ctx.getBean(IAnimalService.class);
		IAlimentService alimentService = ctx.getBean(IAlimentService.class);
		IEnclosService enclosService = ctx.getBean(IEnclosService.class);
		IResponsableService responsableService = ctx.getBean(IResponsableService.class);

		boolean continuer = true;
		int choix = -1;
		while (continuer) {
			System.out.println("choisir une action : ");
			System.out.println("0- arreter");
			System.out.println("1- ajouterAnimal");
			System.out.println("2- listerAnimaux");
			System.out.println("3- recuperer animal par race");
			System.out.println("4- recuperer aliments par animal");
			
			System.out.println("5- mise � jour age animal");
			

			System.out.println("6- ajouterAliment");
			System.out.println("7- listerAliments");
			System.out.println("8- recuperer aliment par nom d'animal");
			
			System.out.println("9- ajouter un enclos");
			System.out.println("10- lister les enclos");
			System.out.println("11- recuperer un enclos par son nom de responsable");
			
			System.out.println("12- ajouter un responsable");
			System.out.println("13- lister les responsable");
			System.out.println("14- recuperer un enclos par son nom de responsable");
			
			choix = sc.nextInt();
			sc.nextLine();

			switch (choix) {
			case 0:
				System.out.println("au revoir");
				continuer = false;
				break;

			case 1:
				System.out.println("saisir un nom d'animal : ");
				System.out.print("> ");
				String nomAnimal = sc.nextLine();
				AnimalDto creerAnimal = animalService.creer(nomAnimal);
				if (creerAnimal == null) {
					System.out.println("erreur creation");
				} else {
					System.out.println("l'animal " + creerAnimal.getId() + " a �t� cr��");
				}
				break;
			case 2:
				List<AnimalDto> lst = animalService.lister();
				System.out.println("la liste des animaux : ");
				lst.forEach(System.out::println);
				break;
			case 3:
				System.out.println("saisir la race  : ");
				String r = sc.nextLine();
				AnimalDto race = animalService.animalParRace(r);
				System.out.println("l'animal : "+race);
				break;
				
			case 4:
				System.out.println("saisir le nom de l'animal  : ");
				String animal = sc.nextLine();
				for (AnimalDto alimentDto : lst) {
					if(animal)
					
				}
				
				List<AlimentDto> liste = alimentService.alimentParAnimal(animal).add(choix, animal);
				break;
				
			case 5:
				System.out.println("saisir l'age Old : ");
				int oldAge = sc.nextInt();

				System.out.println("saisir l'age New: ");
				int newAge = sc.nextInt();

				animalService.miseAjourAge(oldAge,newAge);
				break;
				
			case 6:
				System.out.println("saisir un nom d'aliment : ");
				System.out.print("> ");
				String nomAliment = sc.nextLine();
				AnimalDto creerAliment = animalService.creer(nomAliment);
				if (creerAliment == null) {
					System.out.println("erreur creation");
				} else {
					System.out.println("l'animal " + creerAliment.getId() + " a �t� cr��");
				}
				break;
				
			case 7:
				List<AlimentDto> lst2 = alimentService.lister();
				System.out.println("la liste des aliments : ");
				lst2.forEach(System.out::println);
				break;
				
		
				

			default:
				break;
			}

			System.out.println("\n===========");
		}

		sc.close();
	}
}


