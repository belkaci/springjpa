package com.afpa.entity;

import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Table(name = "t_enclos")
@NamedQueries({ @NamedQuery(name = "listEnclos", query = "SELECT e FROM Enclos e") })
public class Enclos {
	
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id_enclos;


	@Column
	private String nomEnclos;
	
	
	@Column
	private double superficie;
	
	// responsable de l'enclos
	@OneToOne
	private Responsable responsable;
	
	// un enclos est li� � plusieurs animaux
	@OneToMany(fetch = FetchType.LAZY)
	Set<Animal> animaux;
	
	}
