package com.afpa.entity;

import java.util.Set;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Table(name = "t_animal")
@NamedQueries({ @NamedQuery(name = "Animal.findByRaceOrderByRace", query = "SELECT an FROM Animal an where an.race=raceParam "),
	@NamedQuery(name = "Animal.findAll",query = "select an from Animal an")})

public class Animal {
	
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id_animal;

	@Column
	private String race;
	
	@Column
	private int age;
	
	// des animaux sont li�s � plusieurs aliments
	@ManyToMany
	(fetch = FetchType.LAZY,	cascade = { CascadeType.MERGE, CascadeType.PERSIST })
	Set<Aliment> aliments;
	
	// plusieurs animaux sont lies � un seul responsable
	
	@ManyToOne(optional=true)
	private Responsable responsable;
	
	// plusieurs animaux sont lies � un seul enclos
	
	@ManyToOne(optional=true)
	private Enclos enclos;

}


