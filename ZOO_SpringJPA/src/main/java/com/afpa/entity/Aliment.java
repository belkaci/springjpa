package com.afpa.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Table(name = "t_aliment")
@NamedQueries({ @NamedQuery(name = "listAliment", query = "SELECT al FROM Aliment al") })

public class Aliment {


	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id_aliment;

	@Column
	private String nom_aliment;
	
	@Column
	private int quantit�;
	

}
