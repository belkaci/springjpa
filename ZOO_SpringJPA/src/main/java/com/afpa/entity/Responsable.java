package com.afpa.entity;

import java.util.Set;



import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Table(name = "t_responsable")

@NamedQueries({ @NamedQuery(name = "listeResponsable", query = "SELECT r FROM Responsable r") })
@NamedQueries({ @NamedQuery(name = "listeResponsable", query = "SELECT r FROM Responsable r") })
public class Responsable {
	
	
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int identifiant;

	@Column
	private String nom_responsable;

	// le responsable dans mappedBy est le meme que celui dans la classe Enclos 
	@OneToOne 
	//relation 1 � 1 avec enclos
	(cascade = { CascadeType.MERGE, CascadeType.PERSIST }, fetch = FetchType.LAZY, mappedBy = "responsable")
	private Enclos enclos;
	
	// le responsable dans mappedBy est le meme que celui dans la classe Animal 
		@OneToMany
		(cascade = { CascadeType.MERGE, CascadeType.PERSIST }, fetch = FetchType.LAZY, mappedBy = "responsable")
		Set<Animal> animaux;
}