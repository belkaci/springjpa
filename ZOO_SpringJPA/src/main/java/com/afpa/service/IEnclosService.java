package com.afpa.service;

import java.util.List;

import com.afpa.ZooDto.EnclosDto;

public interface IEnclosService {
	
	// je definis les METHODES VIDES propres aux operations concernant l'enclos
	
			EnclosDto creer(String nomEnclos);

			List<EnclosDto> lister();

			EnclosDto enclosParRace(String race);

			void miseAjourAge(double oldSuperficie, int newSuperficie);

		}


