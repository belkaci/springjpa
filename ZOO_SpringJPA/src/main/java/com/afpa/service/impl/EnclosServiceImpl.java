package com.afpa.service.impl;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.ZooDto.EnclosDto;
import com.afpa.dao.IEnclosDao;

import com.afpa.entity.Enclos;
import com.afpa.entity.Enclos;

import com.afpa.service.IEnclosService;

import lombok.extern.slf4j.Slf4j;

//cette classe est annot�e @service====> lors du scan, on va creer un objet bean du type de cette classe
//
@Slf4j
@Service

public class EnclosServiceImpl implements IEnclosService {

	@Autowired
	IEnclosDao enclosDao;

	@Override
	
	// PAS DE return ENTITY dans nos METHODES ====> return List<Enclos> 
	// mais bien objet de type EnclosDto ou List<EnclosDto>
	
	public EnclosDto creer(String nomEnclos) {
		
		List<Enclos> enclos = this.enclosDao.findByNomOrderByNomEnclos(nomEnclos);
		if(enclos != null && enclos.size() != 0) {
			log.error("des enclos ont deja ce nom : " + enclos);
			return null;
		}
		Enclos enclos1 = Enclos.builder().nomEnclos(nomEnclos).build();

		enclos1 = this.enclosDao.save(enclos1);

		return EnclosDto.builder().id_enclos(enclos1.getId_enclos()).build();	
	}
	

	@Override
	
	public List<EnclosDto> lister() {
		Iterator<Enclos> iterator = this.enclosDao.findAllByOrderByNomDesc().iterator();
		List<EnclosDto> lst = new ArrayList<>();
		while(iterator.hasNext()) {
			Enclos x = iterator.next();
			lst.add(EnclosDto.builder()
					.id_enclos(x.getId_enclos())
					.enclos_race(x.getNomEnclos())
					.build());
	}
		return lst;
	}

	@Override
	public EnclosDto enclosParRace(String race) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void miseAjourAge(double oldSuperficie, int newSuperficie) {
		// TODO Auto-generated method stub

	}
}