package com.afpa.service.impl;
import java.util.ArrayList;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.ZooDto.AnimalDto;
import com.afpa.dao.IAnimalDao;
import com.afpa.entity.Animal;
import com.afpa.service.IAnimalService;

import lombok.extern.slf4j.Slf4j;

//cette classe est annot�e @service====> lors du scan, on va creer un objet bean du type de cette classe
//
@Slf4j
@Service

public class AnimalServiceImpl implements IAnimalService {

	@Autowired
	IAnimalDao animalDao;


	// je completent les METHODES presentent dans l'interface IAnimalService
	//(creer, lister,animalParNom,miseAjourNom)

	//je ne peux avoir de retour
	@Override
	public AnimalDto creer(String ma_race) {

		List<AnimalDto> animaux = this.animalDao.findByNomOrderByNomAnimal(ma_race);
		if(animaux != null && animaux.size() != 0) {
			log.error("des animaux ont deja cette race : " + animaux);
			return null;
		}
		Animal animal1 = Animal.builder().race(ma_race).build();

		animal1 = this.animalDao.save(animal1);

		return AnimalDto.builder().id(animal1.getId_animal()).build();		
	}

	@Override
	public List<AnimalDto> lister() {

		Iterator<Animal> iterator = this.animalDao.findAllByOrderByNomDesc().iterator();
		List<AnimalDto> lst = new ArrayList<>();
		while(iterator.hasNext()) {
			Animal x = iterator.next();
			lst.add(AnimalDto.builder()
					.id(x.getId_animal())
					.race(x.getRace())
					.build());		
		}
		return lst;
	}

	@Override
	public AnimalDto animalParRace(String race) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void miseAjourAge(int oldAge, int newAge) {
		// TODO Auto-generated method stub

	}
}
