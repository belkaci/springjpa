package com.afpa.service.impl;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.afpa.ZooDto.AlimentDto;
import com.afpa.dao.IAlimentDao;
import com.afpa.entity.Aliment;
import com.afpa.service.IAlimentService;

import lombok.extern.slf4j.Slf4j;

//cette classe est annot�e @service====> lors du scan, on va creer un objet bean du type de cette classe
//
@Slf4j
@Service

public class AlimentServiceImpl implements IAlimentService {
	
	@Autowired
	IAlimentDao alimentDao;
	
	@Override
	public AlimentDto creer(String nom_aliment) {
		
		List<AlimentDto> aliments = this.alimentDao.findByNomOrderByNomAliment(nom_aliment);
		if(aliments != null && aliments.size() != 0) {
			log.error("des aliments ont deja ce nom : " + aliments);
			return null;
		}
		Aliment aliment1 = Aliment.builder().nom_aliment(nom_aliment).build();

		aliment1 = this.alimentDao.save(aliment1);

		return AlimentDto.builder().id_aliment(aliment1.getId_aliment()).build();	
	}

	@Override
	public List<AlimentDto> lister() {
		
		Iterator<Aliment> iterator = this.alimentDao.findAllByOrderByNomDesc().iterator();
		List<AlimentDto> lst = new ArrayList<>();
		while(iterator.hasNext()) {
			Aliment x = iterator.next();
			lst.add(AlimentDto.builder()
					.id_aliment(x.getId_aliment())
					.nom_aliment(x.getNom_aliment())
					.build());
	}
		return lst;
		
	}

	@Override
	public AlimentDto alimentParAnimal(String nomAnimal) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void miseAjourQuantit�ParAliment(int oldQuantit�, int newQuantit�) {
		// TODO Auto-generated method stub
		
	}

	

}
