package com.afpa.service.impl;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.ZooDto.AnimalDto;
import com.afpa.ZooDto.ResponsableDto;
import com.afpa.dao.IResponsableDao;
import com.afpa.entity.Animal;
import com.afpa.entity.Enclos;
import com.afpa.entity.Responsable;
import com.afpa.service.IResponsableService;

import lombok.extern.slf4j.Slf4j;

//cette classe est annot�e @service====> lors du scan, on va creer un objet bean du type de cette classe
//
@Slf4j
@Service

public class ResponsableServiceImpl implements IResponsableService {
	
	@Autowired
	
	IResponsableDao responsableDao;
	private Object enclosDao;

	// je completent les METHODES presentent dans l'interface IResponsableService
		//(creer, lister,animalParNom,miseAjourNom)
	

	@Override
	public ResponsableDto creer(String nomResponsable,String nomEnclos) {
		
		List<ResponsableDto> responsables = this.responsableDao.findByNom(nomResponsable);
		if(responsables != null && responsables.size() != 0) {
			log.error("des responsables ont deja ce nom : " + responsables.get(0));
			return null;
		}
		Enclos enclos1 = this.enclosDao.findNom(nomEnclos);
		
		if(enclos1 == null) {
			enclos1 = this.enclosDao.save(Enclos.builder().nomEnclos(nomEnclos).build());
		}
		Responsable responsable = Responsable.builder().nom_responsable(nomResponsable).enclos(enclos1).build();
		responsable = this.responsableDao.save(responsable);
		return ResponsableDto.builder().nom(responsable.getNom_responsable()).build();
	}
		
	

	@Override
	public List<ResponsableDto> lister() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponsableDto responsableParEnclos(String nomEnclos) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void miseAjourResponsable(double oldNomResponsable, int newNomResponsable) {
		// TODO Auto-generated method stub
		
	}	

	
}