package com.afpa.service;

import java.util.List;

import com.afpa.ZooDto.AlimentDto;
import com.afpa.ZooDto.AnimalDto;
import com.afpa.ZooDto.ResponsableDto;

public interface IAlimentService {
	// je definis les METHODES VIDES propres aux operations concernant l'aliment
	
	AlimentDto creer(String nomAliment);

	List<AlimentDto> lister();

	List<AlimentDto> alimentParAnimal(String nomAnimal);

	void miseAjourQuantit�ParAliment(int oldQuantit�, int newQuantit�);

}


