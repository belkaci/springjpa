package com.afpa.service;

import java.util.List;

import com.afpa.ZooDto.AnimalDto;

public interface IAnimalService {
	
	// je definis les METHODES VIDES propres aux operations concernant l'animal
	// ces methodes seront appel�es dans le main pour le bean
	
		AnimalDto creer(String race);

		List<AnimalDto> lister();

		AnimalDto animalParRace(String race);

		void miseAjourAge(int oldAge, int newAge);

	}


