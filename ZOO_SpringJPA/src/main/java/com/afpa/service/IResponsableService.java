package com.afpa.service;
import java.util.List;

import com.afpa.ZooDto.ResponsableDto;

public interface IResponsableService {
	

			// je definis les METHODES VIDES propres aux operations concernant le responsable
		
				ResponsableDto creer(String nomResponsable,String nomEnclos);

				List<ResponsableDto> lister();

				ResponsableDto responsableParEnclos(String nomEnclos);

				void miseAjourResponsable(double oldNomResponsable, int newNomResponsable);

			}


